module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
    'Admins',
    [
      {
        email: 'admin@codeverx.com',
        password: 'admin',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ],
    {},
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Admins', null, {}),
};
