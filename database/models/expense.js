module.exports = (sequelize, DataTypes) => {
  const Expense = sequelize.define('Expense', {
    title: DataTypes.STRING,
    amount: DataTypes.TEXT,
    userId: DataTypes.INTEGER,
    resourceId: DataTypes.INTEGER,
    expenseTypeId: DataTypes.INTEGER
  }, {});
  Expense.associate = function(models) {
    Expense.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });

    Expense.belongsTo(models.Resource, {
      foreignKey: 'resourceId',
      as: 'resource',
      onDelete: 'CASCADE',
    });

    Expense.belongsTo(models.expenseType, {
      foreignKey: 'expenseTypeId',
      as: 'expenseType',
      onDelete: 'CASCADE',
    });
  };
  return Expense;
};