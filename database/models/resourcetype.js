'use strict';
module.exports = (sequelize, DataTypes) => {
  const resourceType = sequelize.define('resourceType', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {});
  resourceType.associate = function(models) {
    resourceType.hasMany(models.Resource, {
      foreignKey: 'resourceTypeId',
      as: 'resource',
      onDelete: 'CASCADE',
    });
  };
  return resourceType;
};