module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Resource, {
      foreignKey: 'userId',
      as: 'resource',
      onDelete: 'CASCADE',
    });

    User.hasMany(models.Expense, {
      foreignKey: 'userId',
      as: 'expense',
      onDelete: 'CASCADE',
    });
  };
  return User;
};