'use strict';
module.exports = (sequelize, DataTypes) => {
  const expenseType = sequelize.define('expenseType', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {});
  expenseType.associate = function(models) {
    expenseType.hasMany(models.Expense, {
      foreignKey: 'expenseTypeId',
      as: 'expense',
      onDelete: 'CASCADE',
    });
  };
  return expenseType;
};