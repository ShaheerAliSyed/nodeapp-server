'use strict';
module.exports = (sequelize, DataTypes) => {
  const Resource = sequelize.define('Resource', {
    title: DataTypes.STRING,
    content: DataTypes.STRING,
    amount: DataTypes.TEXT,
    userId: DataTypes.INTEGER,
    resourceTypeId: DataTypes.INTEGER
  }, {});
  Resource.associate = function(models) {
    Resource.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
    Resource.belongsTo(models.resourceType, {
      foreignKey: 'resourceTypeId',
      as: 'resourceType',
      onDelete: 'CASCADE',
    });
  };
  return Resource;
};
