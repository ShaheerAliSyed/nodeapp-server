const models = require('../database/models');

const createExpense= async (req, res) => {
  try {
    const expense = await models.Expense.create(req.body);
    return res.status(201).json({
        expense,
    });
  } catch (error) {
    return res.status(500).json({error: error.message})
  }
}
const getAllExpense  = async (req, res) => {
    try {
      const expense  = await models.Expense.findAll({
        include: [
          {
            model: models.User,
            as: 'user'
          },
          {
            model: models.Resource,
            as: 'resource'
          },
          {
            model: models.expenseType,
            as: 'expenseType'
          },
        ]
      });
      return res.status(200).json({ expense });
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  const getExpenseById = async (req, res) => {
    try {
      const { expenseId } = req.params;
      const expense = await models.Expense.findOne({
        where: { id: expenseId },
        include: [
          {
            model: models.User,
            as: 'user'
          },
          {
            model: models.Resource,
            as: 'resource'
          },
          {
            model: models.expenseType,
            as: 'expenseType'
          },
        ]
      });
      if (expense) {
        return res.status(200).json({ expense });
      }
      return res.status(404).send('expense with the specified ID does not exists');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  const updateExpense = async (req, res) => {
    try {
      const { expenseId } = req.params;
      const [ updated ] = await models.Expense.update(req.body, {
        where: { id: expenseId }
      });
      if (updated) {
        const updatedExpense = await models.Expense.findOne({ where: { id: expenseId } });
        return res.status(200).json({ expense: updatedExpense });
      }
      throw new Error('expense not found');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };

  const deleteExpense = async (req, res) => {
    try {
      const { expenseId } = req.params;
      const deleted = await models.Expense.destroy({
        where: { id: expenseId }
      });
      if (deleted) {
        return res.status(204).send("Expense deleted");
      }
      throw new Error("Expense not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };


module.exports = {
    createExpense,
    getAllExpense,
    getExpenseById,
    updateExpense,
    deleteExpense
}