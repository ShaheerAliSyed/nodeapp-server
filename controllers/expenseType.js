const models = require('../database/models');

const createExpenseType = async (req, res) => {
  try {
    const expensetype = await models.expenseType.create(req.body);
    return res.status(201).json({
        expensetype,
    });
  } catch (error) {
    return res.status(500).json({error: error.message})
  }
}

const getAllExpenseType  = async (req, res) => {
  try {
    const expensetype = await models.expenseType.findAll({
      include: [
      ]
    });
    return res.status(200).json({ expensetype });
  } catch (error) {
    return res.status(500).send(error.message);
  }
}

const getExpenseTypeById = async (req, res) => {
  try {
    const { expenseTypeId } = req.params;
    const expensetype = await models.expenseType.findOne({
      where: { id: expenseTypeId },
      include: [
      ]
    });
    if (expensetype) {
      return res.status(200).json({ expensetype });
    }
    return res.status(404).send('expense type type with the specified ID does not exists');
  } catch (error) {
    return res.status(500).send(error.message);
  }
}

const updateExpenseType = async (req, res) => {
  try {
    const { expenseTypeId } = req.params;
    const [ updated ] = await models.expenseType.update(req.body, {
      where: { id: expenseTypeId }
    });
    if (updated) {
      const updatedExpenseType = await models.expenseType.findOne({ where: { id: expenseTypeId } });
      return res.status(200).json({ expensetype: updatedExpenseType });
    }
    throw new Error('expensetype not found');
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const deleteExpenseType= async (req, res) => {
  try {
    const { expenseTypeId } = req.params;
    const deleted = await models.expenseType.destroy({
      where: { id: expenseTypeId }
    });
    if (deleted) {
      return res.status(204).send("expensetype Type deleted");
    }
    throw new Error("expensetype Type not found");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

module.exports = {
    createExpenseType,
    getAllExpenseType,
    getExpenseTypeById,
    updateExpenseType,
    deleteExpenseType
}