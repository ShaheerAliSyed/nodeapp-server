const models = require('../database/models');

const createResource = async (req, res) => {
  try {
    const resource = await models.Resource.create(req.body);
    return res.status(201).json({
        resource,
    });
  } catch (error) {
    return res.status(500).json({error: error.message})
  }
}
const getAllResource  = async (req, res) => {
    try {
      const resource  = await models.Resource.findAll({
        include: [
          {
            model: models.User,
            as: 'user'
          },
          {
            model: models.resourceType,
            as: 'resourceType'
          },
        ]
      });
      return res.status(200).json({ resource });
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  const getResourceById = async (req, res) => {
    try {
      const { resourceId } = req.params;
      const resource = await models.Resource.findOne({
        where: { id: resourceId },
        include: [
          {
            model: models.resourceType,
            as: 'resourceType'
          },
        ]
      });
      if (resource) {
        return res.status(200).json({ resource });
      }
      return res.status(404).send('resource with the specified ID does not exists');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  const updateResource = async (req, res) => {
    try {
      const { resourceId } = req.params;
      const [ updated ] = await models.Resource.update(req.body, {
        where: { id: resourceId }
      });
      if (updated) {
        const updatedResource = await models.Resource.findOne({ where: { id: resourceId } });
        return res.status(200).json({ resource: updatedResource });
      }
      throw new Error('resource not found');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };

  const deleteResource= async (req, res) => {
    try {
      const { resourceId } = req.params;
      const deleted = await models.Resource.destroy({
        where: { id: resourceId }
      });
      if (deleted) {
        return res.status(204).send("Resource deleted");
      }
      throw new Error("Resource not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };

module.exports = {
    createResource,
    getAllResource,
    getResourceById,
    updateResource,
    deleteResource
}