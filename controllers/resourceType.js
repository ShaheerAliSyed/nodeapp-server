const models = require('../database/models');

const createResourceType = async (req, res) => {
  try {
    const resourcetype = await models.resourceType.create(req.body);
    return res.status(201).json({
        resourcetype,
    });
  } catch (error) {
    return res.status(500).json({error: error.message})
  }
}

const getAllResourceType = async (req, res) => {
    try {
      const resourcetype = await models.resourceType.findAll({
        include: [
        ]
      });
      return res.status(200).json({ resourcetype });
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  const getResourceTypeById = async (req, res) => {
    try {
      const { resourceTypeId } = req.params;
      const resourcetype = await models.resourceType.findOne({
        where: { id: resourceTypeId },
        include: [
        ]
      });
      if (resourcetype) {
        return res.status(200).json({ resourcetype });
      }
      return res.status(404).send('resource type with the specified ID does not exists');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  const updateResourceType = async (req, res) => {
    try {
      const { resourceTypeId } = req.params;
      const [ updated ] = await models.resourceType.update(req.body, {
        where: { id: resourceTypeId }
      });
      if (updated) {
        const updatedResourceType = await models.resourceType.findOne({ where: { id: resourceTypeId } });
        return res.status(200).json({ resourcetype: updatedResourceType });
      }
      throw new Error('resourcetype not found');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };

  const deleteResourceType= async (req, res) => {
    try {
      const { resourceTypeId } = req.params;
      const deleted = await models.resourceType.destroy({
        where: { id: resourceTypeId }
      });
      if (deleted) {
        return res.status(204).send("Resource Type deleted");
      }
      throw new Error("Resource Type not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };



module.exports = {
    createResourceType,
    getAllResourceType,
    getResourceTypeById,
    updateResourceType,
    deleteResourceType
}