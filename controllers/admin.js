const models = require('../database/models');
const  Helper = require('./Helper');

const createLogin= async (req, res) => {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send({'message': 'Some values are missing'});
      }

      if (!Helper.isValidEmail(req.body.email)) {
        return res.status(400).send({ 'message': 'Please enter a valid email address' });
      }

      let admin = await models.Admin.findOne({ where: {email: req.body.email} });
      if (!admin) return res.status(400).send("Email is not registered");
      let adminPass = await models.Admin.findOne({ where: {password: req.body.password} });
      if (!adminPass) return res.status(400).send("Invalid password");
  try {
    const admin = await models.Admin.findOne(req.body);
    return res.status(201).json({
        admin,
    });
  } catch (error) {
    return res.status(500).json({error: error.message})
  }
}


module.exports = {
    createLogin
}
