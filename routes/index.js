const { Router } = require('express');
const cors = require('cors')
const controllers = require('../controllers');
const resourceControllers = require('../controllers/resource');
const expenseControllers = require('../controllers/expense');
const resourceTypeConstrollers = require('../controllers/resourceType');
const expenseTypeConstrollers = require('../controllers/expenseType');
const adminControllers = require('../controllers/admin');

const router = Router();
const corsOptions = {
  origin: 'http://localhost:3000'
}
router.get('/', (req, res) => res.send('Welcome'))

router.post('/users', controllers.createUser);

router.get('/users',cors(corsOptions), controllers.getAllUsers);

router.get('/users/:userId', controllers.getUserById);

router.put('/users/:userId', controllers.updateUser);

router.delete('/users/:userId', controllers.deleteUser);

router.post('/resource', resourceControllers.createResource);

router.get('/resource', resourceControllers.getAllResource);

router.get('/resource/:resourceId', resourceControllers.getResourceById);

router.put('/resource/:resourceId', resourceControllers.updateResource);

router.delete('/resource/:resourceId', resourceControllers.deleteResource);

router.post('/expense', expenseControllers.createExpense);

router.get('/expense', expenseControllers.getAllExpense);

router.get('/expense/:expenseId', expenseControllers.getExpenseById);

router.put('/expense/:expenseId', expenseControllers.updateExpense);

router.delete('/expense/:expenseId', expenseControllers.deleteExpense);

router.get('/resourceType', resourceTypeConstrollers.getAllResourceType);

router.post('/resourceType', resourceTypeConstrollers.createResourceType);

router.get('/resourceType/:resourceTypeId', resourceTypeConstrollers.getResourceTypeById);

router.put('/resourceType/:resourceTypeId', resourceTypeConstrollers.updateResourceType);

router.delete('/resourceType/:resourceTypeId', resourceTypeConstrollers.deleteResourceType);

router.post('/expenseType', expenseTypeConstrollers.createExpenseType);

router.get('/expenseType', expenseTypeConstrollers.getAllExpenseType);

router.get('/expenseType/:expenseTypeId', expenseTypeConstrollers.getExpenseTypeById);

router.put('/expenseType/:expenseTypeId', expenseTypeConstrollers.updateExpenseType);

router.delete('/expenseType/:expenseTypeId', expenseTypeConstrollers.deleteExpenseType);

router.post('/login', cors(corsOptions), adminControllers.createLogin);

module.exports = router;
