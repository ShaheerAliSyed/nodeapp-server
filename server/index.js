const express = require('express');
const cors = require('cors')
const routes = require('../routes');

const server = express();
server.use(express.json());
server.use('/',cors(), routes);
// server.use('/api', routes);

module.exports = server;
